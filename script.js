// Our Services tab

const tabServise = document.querySelectorAll('.service');
const tabDescriptions = document.querySelectorAll('.descriptions')

for(let i = 0; i < tabServise.length; i++) {
    tabServise[i].addEventListener('click', function () {
        document.querySelector('.service.active').classList.remove('active');
        tabServise[i].classList.add('active');

        document.querySelector('.descriptions.active').classList.remove('active');
        let currentDescriptions = document.querySelector(`.descriptions[data-descriptions="${this.dataset.descriptions}"]`);
        currentDescriptions.classList.add('active');
        this.classList.add('active');
    });
}

/*************/


// Our Amazing Work

const tabWork = document.querySelectorAll('.work-tab');
const tabGalleries = document.querySelectorAll('.galleries');
const appendButton = document.querySelector('.btn-work');

for(let a = 0; a < tabWork.length; a++) {
    tabWork[a].addEventListener('click', function () {
        document.querySelector('.work-tab.active').classList.remove('active');
        tabWork[a].classList.add('active');
        document.querySelectorAll('.galleries.active').forEach((el) => el.classList.remove('active'));
        let currentGalleries = [];

        if (this.dataset.work === 'All') {
            let currentGalleries1 = document.querySelectorAll('.galleries');
            for (let q = 0; q < 12; q++) {
                currentGalleries[q] = currentGalleries1[q];
            }
        } else {
            currentGalleries = document.querySelectorAll(`.galleries[data-work="${this.dataset.work}"]`);
            appendButton.classList.remove('hide');
        }

        currentGalleries.forEach((el) => el.classList.add('active'));
        this.classList.add('active');

    });
}

let appendButtonAction = function(btn) {
    let listGalleries = {};
    let activeTab = document.querySelector('.work-tab.active')
        
        if (activeTab.dataset.work === 'All') {
            listGalleries = document.querySelectorAll('.galleries:not(.active)');
        } else {
            listGalleries = document.querySelectorAll(`.galleries[data-work="${this.dataset.work}"]:not(.active)`);
        }

        for (let i = 0; i < 12; i++) {
            if (listGalleries.hasOwnProperty(i)) {
                listGalleries[i].classList.add('active');
            }
        }
    checkShowBtn(btn);
}

function checkShowBtn(btn) {
    let nmbrActive = document.querySelectorAll('.galleries.active').length;
    let nmbrAll  = document.querySelectorAll('.galleries').length;

    if (nmbrActive === nmbrAll) {
        btn.classList.add('hide');
    }
}

/*************/


// What People Say About theHam

const prevButton = document.getElementById("slide-arrow-prev");
const nextButton = document.getElementById("slide-arrow-next");

let slideIndex = 1;

nextButton.addEventListener("click", () => {
    showSlides(slideIndex += 1);
});

prevButton.addEventListener("click", () => {
    showSlides(slideIndex -= 1);
});

let smallPictures = document.querySelectorAll('.photo-slider');
for(let j = 0; j < smallPictures.length; j++) {
    smallPictures[j].addEventListener('click', function () {
        showSlides(slideIndex = j+1)
    })
}

function showSlides(n) {
    let i;
    let slides = document.getElementsByClassName("people-descriptions");
    let photoSlider = document.getElementsByClassName("photo-slider");

    if (n > slides.length) {
        slideIndex = 1;
    } else if (n < 1) {
        slideIndex = slides.length;
    }

    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }

    for (i = 0; i < photoSlider.length; i++) {
        photoSlider[i].className = photoSlider[i].className.replace(" active", "");
    }

    slides[slideIndex-1].style.display = "flex";
    document.querySelector('.photo-slider.sl-active').classList.remove('sl-active');
    photoSlider[slideIndex-1].classList.add("sl-active");
}

/**************/


// Gallery of best images

const grid = document.querySelector('.images-grid');
const msnry = new Masonry(grid, {
    columnWidth: 62,
    itemSelector: '.img-bg',
    gutter: 2,
});

const btnLoad = document.querySelectorAll('.btn-load');
const btnWaves = document.querySelectorAll('.btn-waves');
btnLoad.forEach((btn) => btn.addEventListener('click', function() {
    btn.querySelector('.btn-waves').classList.add('active'),
    setTimeout(function() {
        btn.querySelector('.btn-waves').classList.remove('active'); 
            if (btn.classList.contains('btn-work')) {
                appendButtonAction(btn);
            } else {
                appendBlock(btn)
            }
    }, 2000);
}));


let appendBlock = function(btn) {
    let collectionImgBg = document.querySelectorAll('.img-bg');
    let nmbrCollectionImgBg = collectionImgBg.length;
    let elems = [];
    let fragment = document.createDocumentFragment();

    for (let i = nmbrCollectionImgBg-1; i > nmbrCollectionImgBg-4; i--) {
        let cloneEl = collectionImgBg[i].cloneNode(true);
        fragment.appendChild(cloneEl);
        elems.push(cloneEl);
    }

    grid.appendChild(fragment);
    msnry.appended(elems);
    btn.classList.add('hide');
}

/*************/